# README #


### What is this repository for? ###

* Step-by-step to run a ReactJS Application

### Create new app ###

* Dependency `node >= 6`
* Install create-react-app: `yarn global add create-react-app`
* From some general folder, run `create-react-app app-name` (that will create a new folder with `app-name` name)
* Run the app `cd app-name` then `yarn start`

### Bruno Ban Example ###

We made an exemple with some routing feature, a statefull and a stateless component.

### Run exemple ###

* Clone this repo
* run `yarn install`
* run `yarn start`

### Examples: ###
* css-in-js with media query: src > views > aboutUs
* css-in-css: src > components > header

### Mock ###
* src > views > aboutUs 

### Route ###
* index: src > App.js
* AboutUs: src > views > aboutUs > AboutUs

### Route Title and Meta description (SEO improvement) ###
* React Helmet

### Components ###
* StateLess component: src > components > aboutCardLess
* Statefull component: both view components and src > components > aboutCardFull

