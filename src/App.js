import React, { Component } from 'react';
import Header from './components/header/Header';
import './App.css';

import { Helmet }           from "react-helmet";

class App extends Component {

  componentDidMount = () => {
    console.log(process.env.NODE_ENV);
  }

  render() {
    return (
      <div className="App">

      <Helmet>
        <meta charSet="utf-8" />
        <meta name="description" content="Tutorial sobre algumas utilidades em ReactJS" />
        <title>Bruno Ban - Index</title>
        <link rel="canonical" href="http://mysite.com/example" />
      </Helmet>

        <Header />
        <h2>
          View: App.js
        </h2>

        <div className="desc">
          <p>Os dois primeiros botões utilizam a tag Link para selecionar as rotas.</p>
          <p>Os dois botoes seguintes utilizam uma função que chama o "history.push" para alterar as rotas.</p>

          <p>================================</p>

          <p>Em <strong>Sobre Nós</strong> você encontrará exemplo de utilização de componentes  <strong>Statefull</strong> e <strong>Stateless</strong> </p>

        </div>
      </div>
    );
  }
}

export default App;
