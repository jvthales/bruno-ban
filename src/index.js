import React from 'react';
import ReactDOM from 'react-dom';
import { 
    Router,
    Route,
} from 'react-router-dom';
import './index.css';

import registerServiceWorker from './registerServiceWorker';
import history from './history';
import Routes from './Routes'

import App from './App';
import AboutUs from './views/aboutUs/AboutUs';

ReactDOM.render((
    <Router history={history}>
        <Routes />
    </Router>
), document.getElementById('root'));

registerServiceWorker();
