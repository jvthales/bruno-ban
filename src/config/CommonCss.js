const commonCss = {
    defaultPadding: '30px',
    defaultAlign: 'center',
};

export default commonCss;