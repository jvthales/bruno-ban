if(process.env.NODE_ENV == 'development'){
    module.exports = {
        assetsUrl: 'http://paralisados.com.br',
    }
} else if(process.env.NODE_ENV == 'production') {
    module.exports = {
        assetsUrl: 'http://qualquerRaioDeSite.com',
    }
}
    