import { StyleSheet } from 'aphrodite';
import CommonCss      from '../../config/CommonCss';

const styles = StyleSheet.create({
    commonContainer: {
        paddingBottom: CommonCss.defaultPadding,
        textAlign: CommonCss.defaultAlign,
    },
    titleImage: {
        textAlign: 'center',
        color: '#c0c0c0',
        // This title will be Red in Mobile
        '@media (max-width: 768px)': {
            color: '#d00',
        }
    },
    imageMaxSize: {
        maxWidth: '90%',
    },
    deskOnly: {
        '@media (max-width: 768px)': {
            display: 'none'
        },
    },
    boxComponents: {
        display: 'flex',
        position: 'relative',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
        alignItems: 'center',
        maxWidth: '900px',
        margin: '0px auto',
    }
});

export default styles;