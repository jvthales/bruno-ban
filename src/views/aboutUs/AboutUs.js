import React, { Component } from 'react';
import Header               from '../../components/header/Header';
import { css }              from 'aphrodite';
// css-in-js
import styles               from './styles';
import Configs              from '../../config/Config';

//Helmet - Solução de SEO. Ele cria um header diferente para cada View
import { Helmet }           from "react-helmet";

// components
import AboutCardLess        from '../../components/aboutCardLess/AboutCardLess';
import AboutCardFull        from '../../components/aboutCardFull/AboutCardFull';

// assets
import imgFlag              from '../../assets/imgs/ger-flag.jpg';

// vars
const ragnarImg = `${Configs.assetsUrl}/assets/imgs/ragnar.jpg`;

//mock de resposta do back
const itensAVenda = [
  {
    title: 'Carrinho de paçoca',
    text: 'R$ 12 reais a caixa',
  },
  {
    title: 'Banca do morango',
    text: 'R$ 60 reais o kilo',
  },
  {
    title: 'Banca do abacate',
    text: 'R$ 48 reais o kilo',
  },
  {
    title: 'Banca do caju',
    text: 'R$ 12 reais o kilo',
  },
]


// View - dica. A View é um componente como outro qualquer, que é chamado diretamente por uma rota.
// vários tutoriais deixam as views juntas dos components. Eu separo para facilitar o fluxo de trabalho.
class AboutUs extends Component {

  state = {
    count: 0,
  }

  componentDidMount = () => {
    // esta função executa assim que que o component é carregado
    console.log('env', Configs);
    console.log(process.env.PUBLIC_URL);
  }

  _mapRespostaDoBack = () => {
    return (
      itensAVenda.map((item, index) => {

        return (
          <AboutCardLess
            key={`card-item-${index}`}
            title={item.title}
            text={item.text}
          />
        );

      })
    );
  }

  // por padrão meu, as funções não nativas eu começo com um underline. Assim facilita quando trabalho com funções em níveis diferentes de components.
  _changeCount = (action) => {

    const { count } = this.state;
    let newValue = count + (action == 'add' ? 1 : -1);
    if (newValue < 0) newValue = 0; 
    
    this.setState({
      count: newValue,
    });
    
  } 

  render() {

    const {
      count,
    } = this.state;

    return (
      <div className={css(styles.commonContainer)}>

        <Helmet>
          <meta charSet="utf-8" />
          <meta name="description" content="Tutorial sobre algumas utilidades em ReactJS" />
          <title>Bruno Ban - Sobre Nós</title>
          <link rel="canonical" href="http://mysite.com/example" />
        </Helmet>

        <Header />
        <h2>
          View: AboutUs.js
        </h2>

        <div>
          <div>

            <h3>Exemplo de Components</h3>

            <p>Alguns exemplos de aplicação de components.</p>

            <div className={css(styles.boxComponents)}>
              
              {/* StateLess Component */}
              <AboutCardLess
                title="Bruno Ban na Alemanha"
                img={imgFlag} // optional
                imgTitle="Bandeira da Alemanha" // must have if use img
                text="Bora brincar dom React!!! Bruno Ban finalmente se encontra num lugar de Lord digno dele :D, no velho continente. Está aprendendo de tudo como sempre, e mostrando pro povo da salsicha com mostarda como se trabalha direito. #mandaVerBrunoBan!!"
              />

              <AboutCardLess
                title="Contador de cliques (StateLess)"
                text="Um component Statless é mais simples, performa um pouco melhor (bit escovado). Por isso ele não possui os elementos nativos como State e ComponentDidMount. Mas nada nos impede de passar uma função como propriedade"
                addAction={() => this._changeCount('add')} // optional
                dimAction={() => this._changeCount('dim')} // optional
              />

              {/* StateFull Component */}
              <AboutCardFull
                title="StateFull Contador"
                text="O mesmo component dos anteriores, mas Satefull. Este aqui já tem state interno e consegue trabalhar com as funções nativas do React."
                addAction={() => this._changeCount('add')} // optional
                dimAction={() => this._changeCount('dim')} // optional
              />

            </div>

            <p>Este contador pertence a view AboutUs. Mas por meio do State e Props conseguimos manipular através de Components também.</p>

            <h3>Contador: {count}</h3>

            <p>Botões no próprio component AboutUs (view): <button onClick={() => this._changeCount('dim')}>-</button> <button onClick={() => this._changeCount('add')}>+</button></p>

            {itensAVenda && (
              <div>
                <p>Mock de uma resposta de back. Entenda a função <strong>_mapRespostaDoBack</strong></p>
                <div className={css(styles.boxComponents)}>
                
                  {this._mapRespostaDoBack()}
                
                </div>
              </div>
            )}

            <h3 className={css(styles.titleImage)}>Exemplo de imagem utilizada com variável de ambiente: <span class={css(styles.deskOnly)}>(este título fica vermelho no mobile)</span></h3>

            <img className={css(styles.imageMaxSize)} src={ragnarImg} alt="Ragnar" />

          </div>

        </div>

      </div>
    );
  }
}

export default AboutUs;
