import React from 'react';
import { 
    Route,
} from 'react-router-dom';

import App from './App';
import AboutUs from './views/aboutUs/AboutUs';

const Routes = () => {
    return (
        <div>
            <Route exact path='/' component={App}/>
            <Route path='/sobre-nos' component={AboutUs}/>
        </div>
    )
}

export default Routes;