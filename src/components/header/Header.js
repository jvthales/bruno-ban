import React, {Component} from 'react';
import logo from '../../logo.svg';
import { Link } from 'react-router-dom';
import history from '../../history';

import './style.css';

class Header extends Component {

    // inside function. If you dont want to use arrow function, this one must be binded
    _goToView = (url) => {
        history.push(url);
    }

    render() {
        return (
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h1 className="App-title">Partiu React, Bruno Ban!</h1>

                <div className="header-menu">
                    <div className="flex-item"> 
                        <Link to="/" className="header-menu-button">Home (Link)</Link>
                        <Link to="/sobre-nos" className="header-menu-button">Sobre Nós (Link)</Link>
                    </div>
                    <div className="flex-item"> 
                        <div className="header-menu-button" onClick={() => this._goToView('/')}>Home (Func)</div>
                        <div className="header-menu-button" onClick={() => this._goToView('/sobre-nos')}>Sobre Nós (Func)</div>
                    </div>

                </div>

            </header>
        )
    }
        
}

export default Header;