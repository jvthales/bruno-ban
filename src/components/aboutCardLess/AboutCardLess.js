import React    from 'react';
import { css }  from 'aphrodite';
import styles   from './styles';   

const AboutCardLess = (props) => {
    return (
        <div className={css(styles.card)}>
            <div className={css(styles.inMargin)}>
                <h3 class={css(styles.h3)}>{props.title}</h3>
                {/* O html da imagem só sera carregado se o component trouxer a propriedade de img e imgTitle */}
                {props.img && props.imgTitle && (
                    <img src={props.img} title={props.imgTitle} className={css(styles.img)} />
                )}
                <p class={css(styles.p)}>{props.text}</p>
                {props.dimAction && (
                    <button
                        className={css(styles.button)}
                        onClick={props.dimAction}
                    >-</button>
                )}
                    
                {props.addAction && (
                    <button
                        className={css(styles.button)}
                        onClick={props.addAction}
                    >+</button>
                )}

            </div>
        </div>
    );
}

export default AboutCardLess;