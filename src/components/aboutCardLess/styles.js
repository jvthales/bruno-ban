import { StyleSheet } from 'aphrodite';

const styles = StyleSheet.create({
    card: {
        padding: '20px 0px',
        borderRadius: '5px',
        border: '1px solid #202020',
        maxWidth: '32%',
        marginBottom: '10px',
        '@media (max-width: 768px)': {
            maxWidth: '48%',
        },
        '@media (max-width: 380px)': {
            maxWidth: '90%',
        },
    },
    inMargin: {
        //O mesmo efeito seria obtido se o padding na linha 5 fosse '20px 20px', mas voce teria um bug no crossbrowser da largura.
        margin: '0px 20px',
    },
    h3: {
        fontSize: '16px',
        lineHeight: '20px',
        fontWeight: '700',
        padding: '0 0 8px',
        margin: '0',
    },
    p: {
        fontSize: '14px',
        lineHeight: '18px',
        fontWeight: '300',
        padding: '0',
        margin: '0px 0px 0px',
        textAlign: 'justify',
    },
    img: {
        // limita o tamanho da imagem por css, deixa a altura responsiva e dá respiro ao texto
        maxWidth: '50px',
        height: 'auto',
        marginRight: '5px',
        float: 'left',
    },
    button: {
        margin: '20px 5px auto',
        fontSize: '14px',
        borderRadius: '5px',
        ':hover': {
            background: '#f6f6f6',
        },
    },
})

export default styles;
