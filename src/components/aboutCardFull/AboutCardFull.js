import React, { Component } from 'react';
import { css }              from 'aphrodite';
import styles               from './styles';   

class AboutCardFull extends Component {

    render() {

        const {
            title,
            img,
            imgTitle,
            text,
            dimAction,
            addAction,
        } = this.props;

        return (
            <div className={css(styles.card)}>
                <div className={css(styles.inMargin)}>
                    <h3 class={css(styles.h3)}>{title}</h3>
                    {/* O html da imagem só sera carregado se o component trouxer a propriedade de img e imgTitle */}
                    {img && imgTitle && (
                        <img src={img} title={imgTitle} className={css(styles.img)} />
                    )}
                    <p class={css(styles.p)}>{text}</p>

                    {dimAction && (
                        <button
                            className={css(styles.button)}
                            onClick={dimAction}
                        >-</button>
                    )}
                        
                    {addAction && (
                        <button
                            className={css(styles.button)}
                            onClick={addAction}
                        >+</button>
                    )}

                </div>
            </div>
        );
    }
}

export default AboutCardFull;